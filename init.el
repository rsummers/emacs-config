;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; General Configuration ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Set up the load path and stuff
(require 'package)
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
;;(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))

(package-initialize)

(add-to-list 'load-path "~/.emacs.d/elisp")
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; Add personal info
(setq user-full-name "Randall Summers"
      user-mail-address "rsummers@ou.edu")

;; Define some stuff for location dependent settings
(setq location-is-work-p (equal (getenv "MY_LOCATION") "WORK"))
(setq location-is-home-p (equal (getenv "MY_LOCATION") "HOME"))
(setq location-is-laptop-p (equal (getenv "MY_LOCATION") "LAPTOP"))
(setq location-is-desktop-p (equal (getenv "MY_LOCATION") "DESKTOP"))

;; Move backups to avoid cluttering directories
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))

;; Setup various information bars
(tool-bar-mode -1)
(display-time-mode 1)
(setq display-time-format "%F %H:%M %p")
(setq display-time-default-load-average nil)

;; Hide startup screen
(setq inhibit-startup-screen t)

;; Make sentances end with a single space
(setq sentence-end-double-space nil)

;; Change "yes/no" prompts to "y/n"
(fset 'yes-or-no-p 'y-or-n-p)

;; Set up font and color scheme
(unless  location-is-desktop-p
  (add-to-list 'default-frame-alist '(font . "Source Code Pro-12"))
  (set-face-attribute 'default t :font "Source Code Pro-12"))
(use-package doom-themes
  :config
  (progn
    (setq doom-themes-enable-bold t
	  doom-themes-enable-italic t)
    (load-theme 'doom-nord t)
    (doom-themes-org-config)))

;; Initialize evil mode
(use-package evil
  :config
  (evil-mode 1))

;; Set up Ivy
(use-package counsel
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq ivy-count-format "(%d%d) ")
    (global-set-key (kbd "C-s") 'swiper)
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-x C-f") 'counsel-find-file)))

;; Set up company-mode
(use-package company
  :defer t
  :hook (after-init . global-company-mode))

;; Setup smartparens
(use-package smartparens-config
  :ensure smartparens
  :config
  (progn
    (add-hook 'slime-mode-hook #'smartparens-mode)
    (add-hook 'emacs-lisp-mode-hook #'smartparens-mode)))

;;;;;;;;;
;; Org ;;
;;;;;;;;;

(load "~/.emacs.d/org-mode.el")

;;;;;;;;;;;;;;;;
;; Setup Gnus ;;
;;;;;;;;;;;;;;;;

(setq gnus-init-file "~/.emacs.d/gnus.el")

;;;;;;;;;;;;;;;;;
;; Setup Magit ;;
;;;;;;;;;;;;;;;;;
(use-package magit
  :bind
  (("C-x g" . magit-status)
   ("C-x M-g" . magit-dispatch-popup))
  :config
  (progn
    (if (or location-is-home-p location-is-work-p)
	(setenv "GIT_ASKPASS" "git-gui--askpass"))))

;;;;;;;;;;;;;;;;;;;;;;
;; LISP Development ;;
;;;;;;;;;;;;;;;;;;;;;;

;; Set inferior-lisp-program in device-specific.el
(use-package slime
  :defer t
  :config
  (progn
    (if location-is-laptop-p (load (expand-file-name "~/quicklisp/slime-helper.el")))
    (if location-is-desktop-p (load (expand-file-name "~/quicklisp/slime-helper.el")))
    (setq slime-contribs '(slime-fancy))
    (setq inferior-lisp-program "sbcl")))

(use-package slime-company
  :hook slime-mode
  :init
  (slime-setup '(slime-fancy slime-company)))

;;;;;;;;;;;;;;;;;;;;;;;;
;; Python Development ;;
;;;;;;;;;;;;;;;;;;;;;;;;

(if location-is-home-p
    (progn
      (use-package elpy
	:config
	(elpy-enable))))

;;(if (or location-is-home-p location-is-work-p)
;;    (progn
;;      (use-package conda
;;	:init
;;	(progn
;;	  (setq conda-anaconda-home (expand-file-name "~/Miniconda3"))
;;	  (conda-env-initialize-interactive-shells)
;;	  (conda-env-initialize-eshell)
;;	  (conda-env-autoactivate-mode t))
;;	:config
;;	(progn
;;	  ;(if location-is-home-p
;;	  ;    (conda-env-activate "base"))
;;	  (if location-is-work-p
;;	      (conda-env-activate "rf"))))
;;
;;      (use-package company-jedi
;;	:config
;;	(add-to-list 'company-backends 'company-jedi))))

;;;;;;;;;;;;;;;;;;;;;;;;
;; MATLAB Development ;;
;;;;;;;;;;;;;;;;;;;;;;;;

(use-package matlab
  :defer t
  :ensure matlab-mode
  :config
  (progn
    (add-to-list 'auto-mode-alist '("\\.m$" . matlab-mode))
    (setq matlab-indent-function t)))

;;;;;;;;;;;;;;;;;;;;;
;; Web Development ;;
;;;;;;;;;;;;;;;;;;;;;

;; Initialize Web Mode
(use-package web-mode
  :defer t
  :mode (("\\.html$" . web-mode)
	 ("\\.js$" . web-mode)
	 ("\\.php$" . web-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Document Preparation ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Initialize markdown mode
(use-package markdown-mode
  :defer t
  :mode (("\\.md$" . markdown-mode)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("ce3e6c12b48979ce89754884d913c7ecc8a7956543d8b09ef13abfab6af9aa35" default)))
 '(package-selected-packages
   (quote
    (markdown-mode doom-themes smartparens slime elpy conda magit auctex web-mode org-edna))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
